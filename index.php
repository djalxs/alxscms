<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    
    <title>Simple CMS with PHP</title>
    
  </head>

  <body>
    <div id="page-wrap">
    <?php
    
      require_once('classes/alxsCMS.php');
      $obj = new alxsCMS();
      $admin = 0;
    /* CHANGE THESE SETTINGS FOR YOUR OWN DATABASE */
      $obj->host = 'localhost';
      $obj->username = 'alxsCMS';
      $obj->password = 'amelia2011';
      $obj->table = 'alxsCMS';
      $obj->connect();
    
      if ( $_POST )
        $obj->write($_POST);
      if(isset($_GET['admin'])) {
        $admin = 1;
      }
      echo ( $admin == 1 ) ? $obj->display_admin() : $obj->display_public();
    
    ?>
  </div>
  </body>

</html>